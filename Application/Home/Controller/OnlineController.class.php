<?php
// 本类由系统自动生成，仅供测试用途
namespace Home\Controller;
class OnlineController extends BaseController {
    public function index(){
        $this->assign('title','在线留言');

        $Online = M("Online");

        if($_POST){

        	$data['title'] = $_POST['title'];
			$data['address'] = $_POST['address'];
			$data['name'] = $_POST['name'];
			$data['work'] = $_POST['work'];
			$data['iphone'] = $_POST['iphone'];
			$data['chuanzhen'] = $_POST['chuanzhen'];
			$data['content'] = $_POST['content'];
            $Online->data($data)->add();
        }

        $this->display();
    }
}