<?php
// 本类由系统自动生成，仅供测试用途
namespace Home\Controller;
class ContantController extends BaseController {
    public function index(){
        $this->assign('title','联系我们');
        $this->display();
    }

    public function iframe(){
    	layout(false);
    	$this->display('iframe');
    }
}