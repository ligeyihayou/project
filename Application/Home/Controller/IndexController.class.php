<?php
// 本类由系统自动生成，仅供测试用途
namespace Home\Controller;
class IndexController extends BaseController {
    public function index(){
        $this->assign('title','贵州黔丰汇缘园林绿化有限公司');
        $this->display();
    }

	public function bigBotany(){
    	$this->display('bigBotany');
    }

    public function smallBotany(){
    	$this->display('smallBotany');
    }

    public function huahBotany(){
    	$this->display('huahBotany');
    }

    public function tyBotany(){
    	$this->display('tyBotany');
    }
}
