/**
 * Created by Zheng on 2017/3/15.
 */
$(function () {
    var wow = new WOW({
        boxClass: 'wow',
        animateClass: 'animated',
        offset: 0,
        mobile: true,
        live: true
    });
    wow.init();

    $(".zc-table-con-warrp").find(".zc-table-con:first").show();
    $(".zc-table-wapper .zc-table-item").on("click",function(){ //给a标签添加事件
        var index=$(this).index();  //获取当前a标签的个数
        $(this).addClass('zc-table-item-active').siblings().removeClass('zc-table-item-active');
        console.log(index);
        $('.zc-table-con-warrp .zc-table-con').hide().eq(index).show(); //返回上一层，在下面查找css名为box隐藏，然后选中的显示
        //$(this).addClass("hover").siblings().removeClass("hover"); //a标签显示，同辈元素隐藏
    })
})
