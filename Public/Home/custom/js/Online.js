$(function(){
	// $('#cmy-form-bt').on('click',function(){
	// 	$.ajax({
	//     	type : "POST",
	//     	url : "/Home/Online",   
	//     	success : function (data) {
	//          	console.log(data.req);
	//         } 
	//     });
	// });


	$('#showDataForm').bootstrapValidator({
		title: '请填写标题！！！',
        　feedbackIcons: {
　　　　　　　　valid: 'glyphicon glyphicon-ok',
　　　　　　　　invalid: 'glyphicon glyphicon-remove',
　　　　　　　　validating: 'glyphicon glyphicon-refresh'
　　　　},
        fields: {
			work: {
                message: '请填写单位！！！',
                validators: {
                    notEmpty: {
                        message: '单位不能为空'
                    }
                }
            },         	
			title: {
                message: '请填写标题！！！',
                validators: {
                    notEmpty: {
                        message: '标题不能为空'
                    }
                }
            },        	
            address: {
                message: '请填写地址！！！',
                validators: {
                    notEmpty: {
                        message: '地址不能为空'
                    }
                }
            },
            email: {
            	message: '请填写邮箱！！！',
                validators: {
                    notEmpty: {
                        message: '邮箱地址不能为空'
                    }
                }
            },
            name: {
            	message: '请填写姓名！！！',
                validators: {
                    notEmpty: {
                        message: '姓名不能为空'
                    }
                }
            },
            iphone: {
            	message: '手机号无效！！',
                 validators: {
                     notEmpty: {
                         message: '手机号码不能为空'
                     },
                     stringLength: {
                         min: 11,
                         max: 11,
                         message: '请输入11位手机号码'
                     },
                     regexp: {
                         regexp: /^1[3|5|8]{1}[0-9]{9}$/,
                         message: '请输入正确的手机号码'
                     }
                 }
            },
            chuanzhen: {
                validators: {
                    notEmpty: {
                        message: '传真不能为空'
                    }
                }
            },
            content: {
            	message: '请填写内容！！！',
                validators: {
                    notEmpty: {
                        message: '内容不能为空'
                    }
                },
			   stringLength: {
			    	min: 30,
			    	message: '用户名长度必须大于三十个字！！'
			   }
            }
        }
    })
})	